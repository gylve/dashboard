package api

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/gylve/dashboard/models"
	"gitlab.com/gylve/dashboard/settings"
)

type credentials struct {
	UserName string `json:"username" query:"username" form:"username"`
	Password string `json:"password,omitempty" query:"password" form:"password"`
}

func Login(c echo.Context) (err error) {
	cred := new(credentials)
	if err = c.Bind(cred); err != nil {
		return
	}
	user := &models.Users{}
	result, err := models.ValidatePassword(models.DB, user, cred.UserName, cred.Password, "")
	if err != nil {
		return err
	}
	if !result {
		return echo.ErrUnauthorized
	}
	tokens, err := settings.GenerateTokenPair(cred.UserName)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, tokens)
}
