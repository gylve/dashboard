package api

import (
	"fmt"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/gylve/dashboard/settings"
)

func Refresh(c echo.Context) error {
	type tokenRequestBody struct {
		Refresh string `json: "refresh" query: "refresh"`
	}
	tokenReq := new(tokenRequestBody)
	if err := c.Bind(tokenReq); err != nil {
		return err
	}

	token, err := jwt.Parse(tokenReq.Refresh, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected Signing Method: %v", token.Header["alg"])
		}
		return settings.PublicKey, nil
	})
	if err != nil {
		return err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// Get the user record from database or
		// run through your business logic to verify if the user can
		if claims["Active"] == true {
			username := fmt.Sprintf("%s", claims["UserName"])
			newTokenPair, err := settings.GenerateTokenPair(username)
			if err != nil {
				return err
			}
			return c.JSON(http.StatusOK, newTokenPair)
		}
		return echo.ErrUnauthorized
	}

	return err
}
