package api

import (
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/gylve/dashboard/models"
	"gitlab.com/gylve/dashboard/settings"
)

type userParams struct {
	Username        string `json:"username" query:"username"`
	FirstName       string `json:"first_name" query:"first_name"`
	LastName        string `json:"last_name" query:"last_name"`
	Email           string `json:"email" query:"email"`
	Password        string `json:"password" query:"password"`
	ConfirmPassword string `json:"confirm_password" query:"confirm_password"`
	IsActive        bool   `json:"is_active" query:"is_active"`
	IsEnable        bool   `json:"is_enable" query:"is_enable"`
	IsSuperUser     bool   `json:"is_superuser" query:"is_superuser"`
}

func ListUsers(c echo.Context) (err error) {
	u := &models.Users{}
	if err = models.DB.All(u); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

func FindUser(c echo.Context) (err error) {
	up := new(userParams)
	if err = c.Bind(up); err != nil {
		return err
	}
	u := &models.Users{}
	if err = models.GetUserByUsernameOrEmail(models.DB, u, up.Username, up.Email); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

func CreateUser(c echo.Context) (err error) {
	up := new(userParams)
	if err = c.Bind(up); err != nil {
		return err
	}
	if _, err = models.VerifyPassword(up.Password, up.ConfirmPassword); err != nil {
		return err
	}
	um := &models.User{
		Username:    up.Username,
		FirstName:   up.FirstName,
		LastName:    up.LastName,
		Email:       up.Email,
		Password:    up.Password,
		IsActive:    true,
		IsEnable:    true,
		IsSuperUser: up.IsSuperUser,
	}
	um.SetPassword()
	vErrors, err := models.DB.ValidateAndCreate(um)
	if err != nil {
		return err
	}
	// Return Validation Errors
	if vErrors != nil {
		return vErrors
	}
	return c.JSON(http.StatusCreated, um)
}

func AboutMe(c echo.Context) (err error) {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*settings.JwtCustomClaims)
	u := &models.Users{}
	if err = models.GetUserByUsernameOrEmail(models.DB, u, claims.UserName, ""); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

func UpdateUser(c echo.Context) (err error) {
	up := new(userParams)
	if err = c.Bind(up); err != nil {
		return err
	}
	u := &models.Users{}
	if err = models.GetUserByUsernameOrEmail(models.DB, u, up.Username, ""); err != nil {
		return err
	}
	// Get the user of the slice of users
	// using a pointer selector.
	um := (*u)[0]
	um.FirstName = up.FirstName
	um.LastName = up.LastName
	um.Email = up.Email
	vErrors, err := models.DB.ValidateAndUpdate(&um)
	if err != nil {
		return err
	}
	if vErrors != nil {
		// Return Validation Errors
		return vErrors
	}
	return c.JSON(http.StatusOK, um)
}

// delete endpoint
func DeleteUser(c echo.Context) (err error) {
	up := new(userParams)
	if err = c.Bind(up); err != nil {
		return err
	}
	u := &models.Users{}
	if err = models.GetUserByUsernameOrEmail(models.DB, u, up.Username, ""); err != nil {
		return err
	}
	// Get the user of the slice of users
	// using a pointer selector.
	um := (*u)[0]
	if err = models.DB.Destroy(&um); err != nil {
		return err
	}
	return c.NoContent(http.StatusNoContent)
}
