package settings

import (
	"crypto/rsa"
	"io/ioutil"
	"log"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type JwtCustomClaims struct {
	UserName string `json:username`
	Active   bool   `json:active`
	jwt.StandardClaims
}

var (
	PrivateKey *rsa.PrivateKey
	PublicKey  *rsa.PublicKey
)

func init() {
	var privateBytes, publicBytes []byte
	var err error
	privateBytes, err = ioutil.ReadFile("./private.rsa")
	if err != nil {
		log.Fatal("No se pudo leer la llave privada")
	}
	publicBytes, err = ioutil.ReadFile("./public.rsa.pub")
	if err != nil {
		log.Fatal("No se pudo leer la llave pública")
	}
	PrivateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	PublicKey, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
}

func GenerateTokenPair(userName string) (map[string]string, error) {

	// Set Claims
	claims := &JwtCustomClaims{
		UserName: userName,
		Active:   true,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 12).Unix(),
		},
	}

	// Making JWT Token
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	t, err := token.SignedString(PrivateKey)
	if err != nil {
		return nil, err
	}

	refreshToken := jwt.New(jwt.SigningMethodRS256)
	rtClaims := refreshToken.Claims.(jwt.MapClaims)
	rtClaims["Active"] = true
	rtClaims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	rt, err := refreshToken.SignedString(PrivateKey)
	if err != nil {
		return nil, err
	}

	return map[string]string{
		"access_token":  t,
		"refresh_token": rt,
	}, nil

}
