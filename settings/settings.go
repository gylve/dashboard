package settings

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func MidlewareConfig(e **echo.Echo) {
	(*e).Use(middleware.CORS())
	(*e).Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))
	(*e).Use(middleware.Logger())
	(*e).Use(middleware.Recover())
	(*e).Pre(middleware.AddTrailingSlash())
}
