package main

import (
	"gitlab.com/gylve/dashboard/router"
	"gitlab.com/gylve/dashboard/settings"
)

func main() {
	e := router.New(true)
	settings.MidlewareConfig(&e)
	e.Logger.Fatal(e.Start(":8086"))
}
