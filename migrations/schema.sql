--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: boards; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.boards (
    id uuid NOT NULL,
    name character varying(150) NOT NULL,
    slug character varying(200) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.boards OWNER TO dashboard;

--
-- Name: card_members; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.card_members (
    id uuid NOT NULL,
    card_id uuid NOT NULL,
    user_board_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.card_members OWNER TO dashboard;

--
-- Name: cards; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.cards (
    id uuid NOT NULL,
    list_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.cards OWNER TO dashboard;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.comments (
    id uuid NOT NULL,
    field text,
    card_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.comments OWNER TO dashboard;

--
-- Name: lists; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.lists (
    id uuid NOT NULL,
    name character varying(150) NOT NULL,
    "position" integer NOT NULL,
    board_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.lists OWNER TO dashboard;

--
-- Name: schema_migration; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.schema_migration (
    version character varying(14) NOT NULL
);


ALTER TABLE public.schema_migration OWNER TO dashboard;

--
-- Name: user_boards; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.user_boards (
    id uuid NOT NULL,
    user_id uuid NOT NULL,
    board_id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.user_boards OWNER TO dashboard;

--
-- Name: users; Type: TABLE; Schema: public; Owner: dashboard
--

CREATE TABLE public.users (
    id uuid NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    password character varying(128) NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    is_enable boolean DEFAULT true NOT NULL,
    is_superuser boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO dashboard;

--
-- Name: boards boards_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.boards
    ADD CONSTRAINT boards_pkey PRIMARY KEY (id);


--
-- Name: card_members card_members_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.card_members
    ADD CONSTRAINT card_members_pkey PRIMARY KEY (id);


--
-- Name: cards cards_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: lists lists_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.lists
    ADD CONSTRAINT lists_pkey PRIMARY KEY (id);


--
-- Name: user_boards user_boards_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.user_boards
    ADD CONSTRAINT user_boards_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: boards_name_idx; Type: INDEX; Schema: public; Owner: dashboard
--

CREATE UNIQUE INDEX boards_name_idx ON public.boards USING btree (name);


--
-- Name: boards_slug_idx; Type: INDEX; Schema: public; Owner: dashboard
--

CREATE UNIQUE INDEX boards_slug_idx ON public.boards USING btree (slug);


--
-- Name: schema_migration_version_idx; Type: INDEX; Schema: public; Owner: dashboard
--

CREATE UNIQUE INDEX schema_migration_version_idx ON public.schema_migration USING btree (version);


--
-- Name: users_email_idx; Type: INDEX; Schema: public; Owner: dashboard
--

CREATE UNIQUE INDEX users_email_idx ON public.users USING btree (email);


--
-- Name: users_username_idx; Type: INDEX; Schema: public; Owner: dashboard
--

CREATE UNIQUE INDEX users_username_idx ON public.users USING btree (username);


--
-- Name: card_members card_members_card_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.card_members
    ADD CONSTRAINT card_members_card_id_fkey FOREIGN KEY (card_id) REFERENCES public.cards(id);


--
-- Name: card_members card_members_user_board_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.card_members
    ADD CONSTRAINT card_members_user_board_id_fkey FOREIGN KEY (user_board_id) REFERENCES public.user_boards(id);


--
-- Name: cards cards_list_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.cards
    ADD CONSTRAINT cards_list_id_fkey FOREIGN KEY (list_id) REFERENCES public.lists(id);


--
-- Name: comments comments_card_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_card_id_fkey FOREIGN KEY (card_id) REFERENCES public.cards(id);


--
-- Name: comments comments_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: lists lists_board_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.lists
    ADD CONSTRAINT lists_board_id_fkey FOREIGN KEY (board_id) REFERENCES public.boards(id);


--
-- Name: user_boards user_boards_board_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.user_boards
    ADD CONSTRAINT user_boards_board_id_fkey FOREIGN KEY (board_id) REFERENCES public.boards(id);


--
-- Name: user_boards user_boards_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dashboard
--

ALTER TABLE ONLY public.user_boards
    ADD CONSTRAINT user_boards_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

