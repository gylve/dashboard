package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/gylve/dashboard/api"
	"gitlab.com/gylve/dashboard/settings"
)

func DefaultRoutes(e **echo.Echo) {
	(*e).GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello From Echo")
	})
	(*e).POST("/login/", api.Login)
	(*e).POST("/refresh/", api.Refresh)
	(*e).POST("/register/", api.CreateUser)
}

func RestrictedRoutes(r **echo.Group) {
	config := middleware.JWTConfig{
		Claims:        &settings.JwtCustomClaims{},
		SigningKey:    settings.PublicKey,
		SigningMethod: "RS256",
	}
	(*r).Use(middleware.JWTWithConfig(config))
	(*r).GET("/user/list/", api.ListUsers)
	(*r).GET("/user/", api.FindUser)
	(*r).GET("/me/", api.AboutMe)
	(*r).PUT("/user/", api.UpdateUser)
	(*r).DELETE("/user/", api.DeleteUser)
	//(*r).POST("/user/", api.CreateUser)
}
