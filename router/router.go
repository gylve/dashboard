package router

import (
	"github.com/labstack/echo/v4"
)

func New(debug bool) *echo.Echo {
	e := echo.New()
	e.Debug = debug
	DefaultRoutes(&e)
	r := e.Group("/api/v1")
	RestrictedRoutes(&r)
	return e
}
