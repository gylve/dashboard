module gitlab.com/gylve/dashboard

go 1.15

require (
	github.com/Masterminds/semver/v3 v3.1.0 // indirect
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/alexandrevicenzi/unchained v1.3.0
	github.com/cockroachdb/cockroach-go v2.0.1+incompatible // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gobuffalo/attrs v1.0.0 // indirect
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobuffalo/fizz v1.11.0 // indirect
	github.com/gobuffalo/genny v0.6.0 // indirect
	github.com/gobuffalo/logger v1.0.3
	github.com/gobuffalo/nulls v0.4.0 // indirect
	github.com/gobuffalo/packr/v2 v2.8.0 // indirect
	github.com/gobuffalo/pop v4.13.1+incompatible
	github.com/gobuffalo/pop/v5 v5.2.4
	github.com/gobuffalo/uuid v2.0.5+incompatible
	github.com/gobuffalo/validate v2.0.4+incompatible
	github.com/gobuffalo/validate/v3 v3.3.0 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/gosimple/slug v1.9.0
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.8.0 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/microcosm-cc/bluemonday v1.0.4 // indirect
	github.com/rogpeppe/go-internal v1.6.1 // indirect
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/spf13/cobra v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
	golang.org/x/sys v0.0.0-20200824131525-c12d262b63d8 // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
