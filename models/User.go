package models

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/alexandrevicenzi/unchained"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
)

// User is used by pop to map your .model.Name.Proper.Pluralize.Underscore database table to your go code.
type User struct {
	ID          uuid.UUID   `json:"id" db:"id"`
	CreatedAt   time.Time   `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time   `json:"updated_at" db:"updated_at"`
	Username    string      `json:"username" db:"username"`
	FirstName   string      `json:"first_name" db:"first_name"`
	LastName    string      `json:"last_name" db:"last_name"`
	Email       string      `json:"email" db:"email"`
	Password    string      `json:"password" db:"password"`
	IsActive    bool        `json:"is_active" db:"is_active"`
	IsEnable    bool        `json:"is_enable" db:"is_enable"`
	IsSuperUser bool        `json:"is_superuser" db:"is_superuser"`
	Boards      []Board     `json:"boards,omitempty" has_many:"board"`
	UserBoard   []UserBoard `json:"user_board,omitempty" has_many:"user_board"`
	Comments    []Comment   `json:"comments,omitempty" has_many:"comment"`
}

// String is not required by pop and may be deleted
func (u User) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Users is not required by pop and may be deleted
type Users []User

// String is not required by pop and may be deleted
func (u Users) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (u *User) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{Field: u.Username, Name: "Username"},
		&validators.StringIsPresent{Field: u.FirstName, Name: "FirstName"},
		&validators.StringIsPresent{Field: u.LastName, Name: "LastName"},
		&validators.EmailIsPresent{Field: u.Email, Name: "Email"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (u *User) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (u *User) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// HashPassword : hashing the password using PBKDF2_SHA256
func (u *User) SetPassword() error {
	hash, err := makePassword(u.Password)
	if err != nil {
		return err
	}
	u.Password = hash
	return nil
}

func (u *User) Create(DB *pop.Connection) error {
	err := DB.Create(u)
	return err
}

func makePassword(plainPassword string) (hash string, err error) {
	hash, err = unchained.MakePassword(plainPassword, unchained.GetRandomString(12), "default")
	if err != nil {
		return "", err
	}
	return hash, err
}

func ValidatePassword(DB *pop.Connection, u *Users, username string, password string, email string) (bool, error) {
	sentence := fmt.Sprintf(`username='%s' OR email='%s'`, username, email)
	queryset := DB.Where(sentence)
	queryset.All(u)
	var hash string
	for _, v := range *u {
		if v.Password != "" {
			hash = v.Password
		}
	}
	return checkPassword(password, hash)
}

func checkPassword(password string, hashedPassword string) (result bool, err error) {
	result, err = unchained.CheckPassword(password, hashedPassword)
	return result, err
}

func VerifyPassword(password string, confirmPassword string) (bool, error) {
	if password != "" && confirmPassword != "" {
		if password == confirmPassword {
			return true, nil
		}
	}
	return false, fmt.Errorf("Password mismatch")
}

func GetUserByUsernameOrEmail(DB *pop.Connection, user *Users, username string, email string) (err error) {
	sentence := fmt.Sprintf(`username='%s' OR email='%s'`, username, email)
	q := DB.Where(sentence)
	if err = q.All(user); err != nil {
		return err
	}
	return nil
}
