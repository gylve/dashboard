package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gofrs/uuid"
	"github.com/gosimple/slug"
)

// Board is used by pop to map your .model.Name.Proper.Pluralize.Underscore database table to your go code.
type Board struct {
	ID        uuid.UUID   `json:"id" db:"id"`
	CreatedAt time.Time   `json:"created_at" db:"created_at"`
	UpdatedAt time.Time   `json:"updated_at" db:"updated_at"`
	Name      string      `json:"name" db:"name"`
	Slug      string      `json:"slug" db:"slug"`
	UserBoard []UserBoard `json:"user_board,omitempty" has_many:"user_board"`
	Lists     []List      `json:"list,omitempty" has_many:"lists"`
}

// String is not required by pop and may be deleted
func (b Board) String() string {
	jb, _ := json.Marshal(b)
	return string(jb)
}

// Boards is not required by pop and may be deleted
type Boards []Board

// String is not required by pop and may be deleted
func (b Boards) String() string {
	jb, _ := json.Marshal(b)
	return string(jb)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (b *Board) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (b *Board) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (b *Board) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

func sluglify(value string) string {
	return slug.Make(value)
}
