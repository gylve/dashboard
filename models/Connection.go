package models

import (
	"log"

	"github.com/gobuffalo/pop"
)

// Create a package level variable for a global connection
var DB *pop.Connection

func init() {
	var err error
	DB, err = pop.Connect("development")
	if err != nil {
		log.Fatal(err)
	}
}
