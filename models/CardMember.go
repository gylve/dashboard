package models

import (
	"encoding/json"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gofrs/uuid"
)

// CardMember is used by pop to map your .model.Name.Proper.Pluralize.Underscore database table to your go code.
type CardMember struct {
	ID          uuid.UUID  `json:"id" db:"id"`
	CardID      uuid.UUID  `json:"-" db:"card_id"`
	Card        *Card      `json:"card,omitempty" belongs_to:"card"`
	UserBoardID uuid.UUID  `json:"-" db:"user_board_id"`
	UserBoard   *UserBoard `json:"user_board,omitempty" belongs_to:"user_board"`
	CreatedAt   time.Time  `json:"created_at" db:"created_at"`
	UpdatedAt   time.Time  `json:"updated_at" db:"updated_at"`
}

// String is not required by pop and may be deleted
func (c CardMember) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// CardMembers is not required by pop and may be deleted
type CardMembers []CardMember

// String is not required by pop and may be deleted
func (c CardMembers) String() string {
	jc, _ := json.Marshal(c)
	return string(jc)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (c *CardMember) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (c *CardMember) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (c *CardMember) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
